
// Functions Demo
// Luis Pagan

#include <iostream>
#include <conio.h>

using namespace std;

// function prototype
void sayHello(int count = 1); // count here is optional. "int" needs to match.

int add(int a, int b);;
bool divide(float, float, float&);
void countDownFrom(int number);

int main()
{
	//sayHello(); //default is now "1"

	/*cout << add(3, 6);*/
/*
	float num1 = 0;
	float num2 = 0;

	cout << "Enter two numbers: ";
	cin >> num1;
	cin >> num2;

	float answer = 0;

	if (divide(num1,num2,answer))
	{
		cout << num1 << " divided by " << num2 << " is " << answer;
	}
	else
	{
		cout << "You cannot divide by 0. If not for this error, the world would have ended.";
	}*/

	//int number = 0;

	//cin >> number;

	//countDownFrom(number);

	char input = 'y';
	while (input == 'y')
	{
		countDownFrom(10);

		cout << "again?";
		cin >> input;
	}


	_getch();
	return 0;
}

void sayHello(int count)
{
	for (int i = 0; i < count; i++)
	{
		cout << "Hello!\n";
	}

}

int add(int a, int b)
{
	return a + b;
}

bool divide(float num, float den, float &ans) //pass the answer by reference
{
	if (den == 0) return false;

	ans = num / den;

	num++;
	den += 100;

	return true;

}

void countDownFrom(int number)
{
	cout << number << "\n";
	if (number > 0) countDownFrom(number - 1);

}